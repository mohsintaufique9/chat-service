<?php

use App\Jobs\SendEmail;
use App\Mail\UserRegistered;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('testings', function() {

	$job = (new SendEmail)->delay(Carbon::now()->addSeconds(5));

	dispatch($job);

	return 'email is sent';

});
