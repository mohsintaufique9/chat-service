<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Http\Resources\User\PrivateUserResource;
use App\Mail\UserRegistered;
use App\User;
use Illuminate\Contracts\Mail\queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function register(RegisterUserRequest $request)
    {
    	$user = User::create($request->only('email', 'username', 'password'));

    	$email = $user->email;

    	Mail::to($email)->queue(new UserRegistered());

    	return new PrivateUserResource($user);
    }
}
