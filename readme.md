php dependencies
laravel 5.7
laravel horizon
laravel-pdf (mpdf)
laravel-passport
L5-repository
laravel-modules 

infrastructure
docker containers:
 laravel-echo
 redis
 mysql/mariadb
 nginx

Database
db schema with laravel migrations