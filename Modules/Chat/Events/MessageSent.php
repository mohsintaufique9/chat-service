<?php

namespace Modules\Chat\Events;

use App\User;
use Illuminate\Queue\SerializesModels;
use Modules\Chat\Entities\Message;

class MessageSent
{
    use SerializesModels;

    public $user;

    /**
     * Message details
     *
     * @var Message
     */
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Message $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-service');
    }
    
}
