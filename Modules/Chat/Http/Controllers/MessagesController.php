<?php

namespace Modules\Chat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Chat\Entities\Message;
use Modules\Chat\Events\MessageSent;

class MessagesController extends Controller
{
    
    
    public function index()
    {
        return Message::with('user')->get();
    }


    public function store(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
        
    }

    public function sendMessage(Request $request)
    {
        dd($request->all());
    }

}
